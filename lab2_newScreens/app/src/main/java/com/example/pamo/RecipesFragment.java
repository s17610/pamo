package com.example.pamo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.pamo.databinding.FragmentRecipesBinding;
import com.google.android.material.tabs.TabLayout;

public class RecipesFragment extends Fragment {

    private TextView ingredients;
    private TextView recipeSteps;
    private TabLayout tabs;
    private ImageView pizza;
    private ImageView fries;

    private FragmentRecipesBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentRecipesBinding.inflate(inflater, container, false);

        // get references to programmatically manipulated TextViews
        ingredients = binding.ingredientsContentTextView;
        recipeSteps = binding.recipeContentTextView;
        tabs = binding.tabLayout;
        pizza = binding.pizzaImageView;
        fries = binding.frenchFriesImageView;

        // tab layout listener
        tabs.addOnTabSelectedListener(tabsListener);

        return binding.getRoot();
    }

    //tab layout listener method
    private final TabLayout.OnTabSelectedListener tabsListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            if (tab.getPosition() == 0) {
                ingredients.setText(R.string.pizza_ingredients);
                recipeSteps.setText(R.string.pizza_steps);
                fries.setVisibility(View.GONE);
                pizza.setVisibility(View.VISIBLE);
            }
            else {
                ingredients.setText(R.string.french_fries_ingredients);
                recipeSteps.setText(R.string.french_fries_steps);
                fries.setVisibility(View.VISIBLE);
                pizza.setVisibility(View.GONE);
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}