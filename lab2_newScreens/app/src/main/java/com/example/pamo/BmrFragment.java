package com.example.pamo;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.pamo.databinding.FragmentBmrBinding;

import java.text.NumberFormat;

public class BmrFragment extends Fragment {

    private double weight = 0.0; // weight (kg) entered by the user
    private int height = 0; // height (cm) entered by the user
    private int age = 0; // age (years) entered by the user
    private boolean sex = false; // sex (false = male | true = female)
    private double bmr = 0.0; // calculated BMR value
    private TextView label; // BMR label text
    private TextView result; // BMR result
    private Switch sexSwitch;

    private FragmentBmrBinding binding;

    // number formatter object
    private static final NumberFormat numberFormat =
            NumberFormat.getNumberInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentBmrBinding.inflate(inflater, container, false);

        // get references to programmatically manipulated TextViews
        label = binding.bmrLablelTextView;
        result = binding.bmrResultTextView;
        Button btn = binding.bmrCalculateButton;
        sexSwitch = binding.bmrSexSwitch;
        EditText weightText = binding.bmrWeightEditTextNumberDecimal;
        EditText heightText = binding.bmrHeightEditTextNumber;
        EditText ageText = binding.bmrAgeEditTextNumber;

        // set listeners
        btn.setOnClickListener(btnClick);
        sexSwitch.setOnCheckedChangeListener(switchClick);
        weightText.addTextChangedListener(weightTextListener);
        heightText.addTextChangedListener(heightTextListener);
        ageText.addTextChangedListener(ageTextListener);

        return binding.getRoot();
    }

    //BMR calculate method
    private void calculate() {

        //calculate BMR
        if (sex) {
            bmr = 655.1 +(9.563 * weight) + (1.850 * height) - (4.676 * age);
        }
        else {
            bmr = 66.5 + (13.75 * weight) + (5.003 * height) - (6.75 * age);
        }

        // display BMR
        result.setText(numberFormat.format(bmr));

    }

    //Calculate listener
    private final View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick (View view) {
            calculate();
            label.setVisibility(View.VISIBLE);
            result.setVisibility(View.VISIBLE);
        }
    };

    //Sex switch listener
    private final CompoundButton.OnCheckedChangeListener switchClick =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.v("Switch State =", "" + isChecked);
                    if (isChecked) {
                        sexSwitch.setText(R.string.bmr_sex_female);
                        sex = true;
                    }
                    else {
                        sexSwitch.setText(R.string.bmr_sex_male);
                        sex = false;
                    }
                }
            };

    // ageEditText listener
    private final TextWatcher ageTextListener = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try {
                age = Integer.parseInt(s.toString());
                Log.v("Age =", "" + age);
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                age = 0;
            }

        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    // heightEditText listener
    private final TextWatcher heightTextListener = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try {
                height = Integer.parseInt(s.toString());
                Log.v("Height =", "" + height);
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                height = 0;
            }

        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    // weightEditText listener
    private final TextWatcher weightTextListener = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try {
                weight = Double.parseDouble(s.toString());
                Log.v("Weight =", "" + weight);
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                weight = 0.0;
            }

        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}