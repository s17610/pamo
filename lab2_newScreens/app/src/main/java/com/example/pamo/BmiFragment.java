package com.example.pamo;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.pamo.databinding.FragmentBmiBinding;

import java.text.NumberFormat;

public class BmiFragment extends Fragment {

    private FragmentBmiBinding binding;

    // number formatter object
    private static final NumberFormat numberFormat =
            NumberFormat.getNumberInstance();

    private double weight = 0.0; // weight (kilograms) entered by the user
    private double height = 0.0; // height (meters) entered by the user
    private TextView weightTextView; // shows formatted weight value
    private TextView heightTextView; // shows formatted height value
    private TextView bmiTextView; // shows calculated BMI

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentBmiBinding.inflate(inflater, container, false);

        // get references to programmatically manipulated TextViews
        weightTextView = binding.weightTextView;
        heightTextView = binding.heightTextView;
        bmiTextView = binding.bmiTextView;
        bmiTextView.setText(numberFormat.format(0.00));

        // set weightEditText's TextWatcher
        EditText weightEditText = binding.weightEditText;
        weightEditText.addTextChangedListener(weightEditTextWatcher);

        // set heightEditText's TextWatcher
        EditText heightEditText = binding.heightEditText;
        heightEditText.addTextChangedListener(heightEditTextWatcher);

        return binding.getRoot();
    }

    // calculate and display BMI value
    private void calculate() {

        // calculate BMI
        double bmi = weight / (height * height);

        // display BMI
        bmiTextView.setText(numberFormat.format(bmi));
    }

    // listener object for the EditText's text-changed events
    private final TextWatcher weightEditTextWatcher = new TextWatcher() {
        // called when the user modifies the weight value
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try { // get weight value and display number formatted value
                weight = Double.parseDouble(s.toString()) / 100.0;
                weightTextView.setText(numberFormat.format(weight));
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                weightTextView.setText("");
                weight = 0.0;
            }

            calculate(); // update the bmiTextView
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    private final TextWatcher heightEditTextWatcher = new TextWatcher() {
        // called when the user modifies the height value
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try { // get height value and display number formatted value
                height = Double.parseDouble(s.toString()) / 100.0;
                heightTextView.setText(numberFormat.format(height));
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                heightTextView.setText("");
                height = 0.0;
            }

            calculate(); // update the bmiTextView
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}