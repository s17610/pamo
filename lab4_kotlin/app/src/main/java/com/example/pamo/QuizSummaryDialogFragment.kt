package com.example.pamo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment

class QuizSummaryDialogFragment : DialogFragment() {
    private var quizSummary: TextView? = null
    private var quizSummaryContent: TextView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_quiz_summary, container, false)
        val resetQuizBtn = view.findViewById<TextView>(R.id.resetQuizBtn)
        quizSummary = view.findViewById(R.id.quizSummaryTextView)
        quizSummaryContent = view.findViewById(R.id.quizSummaryContentTextView)
        resetQuizBtn.setOnClickListener {
            resetQuizBtn.setOnClickListener {
                Log.e("resetQuizBtn", "Clicked resetQuizBtn") }
        }
        return view
    }
}