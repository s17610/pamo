package com.example.pamo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.pamo.databinding.FragmentBmrBinding
import java.text.NumberFormat

class BmrFragment : Fragment() {
    private var weight = 0.0 // weight (kg) entered by the user
    private var height = 0 // height (cm) entered by the user
    private var age = 0 // age (years) entered by the user
    private var sex = false // sex (false = male | true = female)
    private var bmr = 0.0 // calculated BMR value
    private var label // BMR label text
            : TextView? = null
    private var result // BMR result
            : TextView? = null
    private var sexSwitch: Switch? = null
    private var binding: FragmentBmrBinding? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentBmrBinding.inflate(inflater, container, false)

        // get references to programmatically manipulated TextViews
        label = binding!!.bmrLablelTextView
        result = binding!!.bmrResultTextView
        val btn = binding!!.bmrCalculateButton
        sexSwitch = binding!!.bmrSexSwitch
        val weightText = binding!!.bmrWeightEditTextNumberDecimal
        val heightText = binding!!.bmrHeightEditTextNumber
        val ageText = binding!!.bmrAgeEditTextNumber

        // set listeners
        btn.setOnClickListener(btnClick)
        sexSwitch!!.setOnCheckedChangeListener(switchClick)
        weightText.addTextChangedListener(weightTextListener)
        heightText.addTextChangedListener(heightTextListener)
        ageText.addTextChangedListener(ageTextListener)
        return binding!!.root
    }

    //BMR calculate method
    private fun calculate() {

        //calculate BMR
        bmr = if (sex) {
            655.1 + 9.563 * weight + 1.850 * height - 4.676 * age
        } else {
            66.5 + 13.75 * weight + 5.003 * height - 6.75 * age
        }

        // display BMR
        result!!.text = numberFormat.format(bmr)
    }

    //Calculate listener
    private val btnClick = View.OnClickListener {
        calculate()
        label!!.visibility = View.VISIBLE
        result!!.visibility = View.VISIBLE
    }

    //Sex switch listener
    private val switchClick = CompoundButton.OnCheckedChangeListener { _, isChecked ->
        Log.v("Switch State =", "" + isChecked)
        sex = if (isChecked) {
            sexSwitch!!.setText(R.string.bmr_sex_female)
            true
        } else {
            sexSwitch!!.setText(R.string.bmr_sex_male)
            false
        }
    }

    // ageEditText listener
    private val ageTextListener: TextWatcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int,
                                   before: Int, count: Int) {
            try {
                age = s.toString().toInt()
                Log.v("Age =", "" + age)
            } catch (e: NumberFormatException) { // if s is empty or non-numeric
                age = 0
            }
        }

        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int, after: Int) {
        }
    }

    // heightEditText listener
    private val heightTextListener: TextWatcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int,
                                   before: Int, count: Int) {
            try {
                height = s.toString().toInt()
                Log.v("Height =", "" + height)
            } catch (e: NumberFormatException) { // if s is empty or non-numeric
                height = 0
            }
        }

        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int, after: Int) {
        }
    }

    // weightEditText listener
    private val weightTextListener: TextWatcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int,
                                   before: Int, count: Int) {
            try {
                weight = s.toString().toDouble()
                Log.v("Weight =", "" + weight)
            } catch (e: NumberFormatException) { // if s is empty or non-numeric
                weight = 0.0
            }
        }

        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int, after: Int) {
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        // number formatter object
        private val numberFormat = NumberFormat.getNumberInstance()
    }
}