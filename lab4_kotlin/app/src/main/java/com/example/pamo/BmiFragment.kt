package com.example.pamo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.pamo.databinding.FragmentBmiBinding
import java.text.NumberFormat

class BmiFragment : Fragment() {
    private var binding: FragmentBmiBinding? = null
    private var weight = 0.0 // weight (kilograms) entered by the user
    private var height = 0.0 // height (meters) entered by the user
    private var weightTextView // shows formatted weight value
            : TextView? = null
    private var heightTextView // shows formatted height value
            : TextView? = null
    private var bmiTextView // shows calculated BMI
            : TextView? = null
    private var bmiFrameLayout: FrameLayout? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentBmiBinding.inflate(inflater, container, false)

        // get references to programmatically manipulated TextViews
        weightTextView = binding!!.weightTextView
        heightTextView = binding!!.heightTextView
        bmiTextView = binding!!.bmiTextView
        bmiTextView!!.text = numberFormat.format(0.00)
        val bmiWebView = binding!!.bmiChartWebView
        val btn = binding!!.bmiChartButton
        bmiFrameLayout = binding!!.bmiFrameLayout

        //set listeners
        btn.setOnClickListener(btnClick)

        //enabling JavaScript for WebView
        val webSettings = bmiWebView.settings
        webSettings.javaScriptEnabled = true

        // Create an unencoded HTML string
        // then convert the unencoded HTML string into bytes, encode
        // it with Base64, and load the data.
        val unencodedHtml = """<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

          var data = new google.visualization.DataTable();
          data.addColumn('date', 'Time of Day');
          data.addColumn('number', 'BMI');

          data.addRows([
            [new Date(2022, 0, 1), 24.9],
            [new Date(2022, 0, 2), 24.9],
            [new Date(2022, 0, 3), 25],
            [new Date(2022, 0, 4), 25.1],
            [new Date(2022, 0, 5), 25.2],
            [new Date(2022, 0, 6), 25.2],
            [new Date(2022, 0, 7), 25.3],
            [new Date(2022, 0, 8), 25.4],
            [new Date(2022, 0, 9), 25.5],
            [new Date(2022, 0, 10), 25.6],
            [new Date(2022, 0, 11), 25.6],
            [new Date(2022, 0, 12), 25.6],
            [new Date(2022, 0, 13), 25.7],
            [new Date(2022, 0, 14), 25.9],
            [new Date(2022, 0, 15), 26],
            [new Date(2022, 0, 16), 26.1],
            [new Date(2022, 0, 17), 26.1],
            [new Date(2022, 0, 18), 26.1],
            [new Date(2022, 0, 19), 26.1],
            [new Date(2022, 0, 20), 26.3],
            [new Date(2022, 0, 21), 26.4],
            [new Date(2022, 0, 22), 26.5],
            [new Date(2022, 0, 23), 26.5],
            [new Date(2022, 0, 24), 26.4],
            [new Date(2022, 0, 25), 26.3],
            [new Date(2022, 0, 26), 26.3],
            [new Date(2022, 0, 27), 26],
            [new Date(2022, 0, 28), 25.9],
            [new Date(2022, 0, 29), 25.6],
            [new Date(2022, 0, 30), 25.6],
            [new Date(2022, 0, 31), 25.3],
            [new Date(2022, 1, 1), 25],
            [new Date(2022, 1, 2), 25.2]
          ]);

          var options = {
            title: 'Zmiana BMI w czasie',
            width: 400,
            height: 500,
            hAxis: {
              format: 'MMM dd, yyyy',
              gridlines: {
                count: 15
              }
            },
            vAxis: {
              gridlines: {
                color: 'none'
              },
              minValue: 20
            }
          };

          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

          chart.draw(data, options);

        }
    </script>
  </head>
  <body>
    <div id="chart_div"></div>
  </body>
</html>"""
        val encodedHtml = Base64.encodeToString(unencodedHtml.toByteArray(),
                Base64.NO_PADDING)
        bmiWebView.loadData(encodedHtml, "text/html", "base64")

        // set weightEditText's TextWatcher
        val weightEditText = binding!!.weightEditText
        weightEditText.addTextChangedListener(weightEditTextWatcher)

        // set heightEditText's TextWatcher
        val heightEditText = binding!!.heightEditText
        heightEditText.addTextChangedListener(heightEditTextWatcher)
        return binding!!.root
    }

    //Button listener
    private val btnClick = View.OnClickListener { bmiFrameLayout!!.visibility = View.VISIBLE }

    // calculate and display BMI value
    private fun calculate() {

        // calculate BMI
        val bmi = weight / (height * height)

        // display BMI
        bmiTextView!!.text = numberFormat.format(bmi)
    }

    // listener object for the EditText's text-changed events
    private val weightEditTextWatcher: TextWatcher = object : TextWatcher {
        // called when the user modifies the weight value
        override fun onTextChanged(s: CharSequence, start: Int,
                                   before: Int, count: Int) {
            try { // get weight value and display number formatted value
                weight = s.toString().toDouble() / 100.0
                weightTextView!!.text = numberFormat.format(weight)
            } catch (e: NumberFormatException) { // if s is empty or non-numeric
                weightTextView!!.text = ""
                weight = 0.0
            }
            calculate() // update the bmiTextView
        }

        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int, after: Int) {
        }
    }
    private val heightEditTextWatcher: TextWatcher = object : TextWatcher {
        // called when the user modifies the height value
        override fun onTextChanged(s: CharSequence, start: Int,
                                   before: Int, count: Int) {
            try { // get height value and display number formatted value
                height = s.toString().toDouble() / 100.0
                heightTextView!!.text = numberFormat.format(height)
            } catch (e: NumberFormatException) { // if s is empty or non-numeric
                heightTextView!!.text = ""
                height = 0.0
            }
            calculate() // update the bmiTextView
        }

        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int, after: Int) {
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        // number formatter object
        private val numberFormat = NumberFormat.getNumberInstance()
    }
}