package com.example.pamo

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.pamo.databinding.FragmentQuizBinding
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlin.math.max

abstract class QuizFragment : Fragment() {
    private var shakeAnimation // animation for incorrect guess
            : Animation? = null
    private var handler // used to delay loading next flag
            : Handler? = null
    private var quizLinearLayout // layout that contains the quiz
            : LinearLayout? = null
    abstract var guessLinearLayouts // rows of answer Buttons
            : Array<LinearLayout?>
    private var questionNumberTextView // shows current question #
            : TextView? = null
    private var questionContentTextView // shows current question content
            : TextView? = null
    private var answerTextView // displays correct answer
            : TextView? = null
    private var totalGuesses // number of guesses made
            = 0
    private var correctAnswers // number of correct guesses
            = 0
    private var questions // list of quiz questions
            : List<Question>? = null
    private var binding: FragmentQuizBinding? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentQuizBinding.inflate(inflater, container, false)

        // load the shake animation that's used for incorrect answers
        val shakeAnimation = AnimationUtils.loadAnimation(activity, R.anim.incorrect_shake)
        shakeAnimation.repeatCount = 3 // animation repeats 3 times
        handler = Handler()

        // get references to programmatically manipulated TextViews
        quizLinearLayout = binding!!.quizLinearLayout
        guessLinearLayouts = arrayOfNulls(2)
        guessLinearLayouts[0] = binding!!.row1LinearLayout
        guessLinearLayouts[1] = binding!!.row2LinearLayout
        questionNumberTextView = binding!!.questionNumberTextView
        questionContentTextView = binding!!.questionContentTextView
        answerTextView = binding!!.answerTextView

        // configure listeners for the guess Buttons
        for (row in guessLinearLayouts) {
            for (column in 0 until row!!.childCount) {
                val button = row.getChildAt(column) as Button
                button.setOnClickListener(guessButtonListener)
            }
        }

        // getting questions data from JSON file
        val jsonFileString = Utils.getJsonFromAssets(context, "quiz_questions.json")
        Log.i("quiz_questions_data", jsonFileString!!)
        val gson = Gson()
        val listQuestionType = object : TypeToken<List<Question?>?>() {}.type
        questions = gson.fromJson(jsonFileString, listQuestionType)

        // set questionNumberTextView's text
        questionNumberTextView!!.text = getString(R.string.question, 1, questions!!.size)
        startQuiz()
        return binding!!.root
    }

    // set up and start the quiz
    private fun startQuiz() {
        correctAnswers = 0 // reset the number of correct answers made
        totalGuesses = 0 // reset the total number of guesses the user made
        loadNextQuestion() // start the quiz by loading the first flag
    }

    // after the user guesses a correct flag, load the next flag
    private fun loadNextQuestion() {
        questionContentTextView!!.text = "" // clear questionContentTextView
        answerTextView!!.text = "" // clear answerTextView

        // display current question number
        questionNumberTextView!!.text = getString(
                R.string.question, correctAnswers + 1, questions!!.size)

        // display current question content
        questionContentTextView!!.text = questions!![correctAnswers].question

        // add 2, 4, 6 or 8 guess Buttons based on the value of GUESS_ROWS
        for (row in 0 until GUESS_ROWS) {
            // place Buttons in currentTableRow
            for (column in 0 until guessLinearLayouts[row]!!.childCount) {
                // get reference to Button to configure
                val newGuessButton = guessLinearLayouts[row]!!.getChildAt(column) as Button
                newGuessButton.isEnabled = true
                // set answers as newGuessButton's text
                newGuessButton.text = questions!![correctAnswers].answers!![row * 2 + column]
            }
        }
    }

    // utility method that disables all answer Buttons
    private fun disableButtons() {
        for (row in 0 until GUESS_ROWS) {
            val guessRow = guessLinearLayouts[row]
            for (i in 0 until guessRow!!.childCount) guessRow.getChildAt(i).isEnabled = false
        }
    }

    // animates the entire quizLinearLayout on or off screen
    private fun animate(animateOut: Boolean) {
        // prevent animation into the the UI for the first flag
        if (correctAnswers == 0) return

        // calculate center x and center y
        val centerX = (quizLinearLayout!!.left +
                quizLinearLayout!!.right) / 2 // calculate center x
        val centerY = (quizLinearLayout!!.top +
                quizLinearLayout!!.bottom) / 2 // calculate center y

        // calculate animation radius
        val radius = max(quizLinearLayout!!.width,
                quizLinearLayout!!.height)
        val animator: Animator

        // if the quizLinearLayout should animate out rather than in
        if (animateOut) {
            // create circular reveal animation
            animator = ViewAnimationUtils.createCircularReveal(
                    quizLinearLayout, centerX, centerY, radius.toFloat(), 0f)
            animator.addListener(
                    object : AnimatorListenerAdapter() {
                        // called when the animation finishes
                        override fun onAnimationEnd(animation: Animator) {
                            loadNextQuestion()
                        }
                    }
            )
        } else { // if the quizLinearLayout should animate in
            animator = ViewAnimationUtils.createCircularReveal(
                    quizLinearLayout, centerX, centerY, 0f, radius.toFloat())
        }
        animator.duration = 500 // set animation duration to 500 ms
        animator.start() // start the animation
    }

    // called when a guess Button is touched
    private val guessButtonListener = View.OnClickListener { v ->
        val guessButton = v as Button
        val guess = guessButton.text.toString()
        val answer = questions!![correctAnswers].correct_answer
        ++totalGuesses // increment number of guesses the user has made
        if (guess == answer) { // if the guess is correct
            ++correctAnswers // increment the number of correct answers

            // display correct answer in green text
            answerTextView!!.text = "$answer!"
            answerTextView!!.setTextColor(
                    resources.getColor(R.color.correct_answer,
                            requireContext().theme))
            disableButtons() // disable all guess Buttons

            // if the user has correctly answered all questions
            if (correctAnswers == questions!!.size) {
                // DialogFragment to display quiz stats and start quiz
                val quizResults: DialogFragment = QuizSummaryDialogFragment()
                quizResults.isCancelable = false
                quizResults.show(childFragmentManager, "quiz results")

/*                    DialogFragment quizResults =
                            new QuizSummaryDialogFragment() {
                                // create an AlertDialog and return it
                                @NonNull
                                @Override
                                public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
                                    return new AlertDialog.Builder(requireContext())
                                            .setMessage(getString(R.string.results,
                                                    totalGuesses, (1000 / (double) totalGuesses)))
                                            .setPositiveButton(R.string.reset_quiz,
                                                    (dialog, id) -> startQuiz())
                                            .create();
                                }
                            };*/

                // use FragmentManager to display the DialogFragment
                //quizResults.setCancelable(false);
                //quizResults.show(getChildFragmentManager(), "quiz results");
            } else { // answer is correct but quiz is not over
                // load the next flag after a 2-second delay
                handler!!.postDelayed(
                        {
                            animate(true) // animate the question off the screen
                        }, 2000) // 2000 milliseconds for 2-second delay
            }
        } else { // answer was incorrect
            questionContentTextView!!.startAnimation(shakeAnimation) // play shake

            // display "Incorrect!" in red
            answerTextView!!.setText(R.string.incorrect_answer)
            answerTextView!!.setTextColor(resources.getColor(
                    R.color.incorrect_answer, requireContext().theme))
            guessButton.isEnabled = false // disable incorrect answer
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        private const val GUESS_ROWS = 2
    }
}