package com.example.pamo

import android.content.Context
import java.io.IOException
import java.nio.charset.Charset

object Utils {
    fun getJsonFromAssets(context: Context?, fileName: String?): String? {
        val jsonString: String = try {
            val `is` = context!!.assets.open(fileName!!)
            val size = `is`.available()
            val buffer = ByteArray(size)
            val charset: Charset = Charsets.UTF_8
            `is`.read(buffer)
            `is`.close()
            String(buffer, charset)
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return jsonString
    }
}