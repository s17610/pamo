package com.example.pamo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.pamo.databinding.FragmentRecipesBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener

class RecipesFragment : Fragment() {
    private var ingredients: TextView? = null
    private var recipeSteps: TextView? = null
    private var tabs: TabLayout? = null
    private var pizza: ImageView? = null
    private var fries: ImageView? = null
    private var binding: FragmentRecipesBinding? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentRecipesBinding.inflate(inflater, container, false)

        // get references to programmatically manipulated TextViews
        ingredients = binding!!.ingredientsContentTextView
        recipeSteps = binding!!.recipeContentTextView
        tabs = binding!!.tabLayout
        pizza = binding!!.pizzaImageView
        fries = binding!!.frenchFriesImageView

        // tab layout listener
        tabs!!.addOnTabSelectedListener(tabsListener)
        return binding!!.root
    }

    //tab layout listener method
    private val tabsListener: OnTabSelectedListener = object : OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab) {
            if (tab.position == 0) {
                ingredients!!.setText(R.string.pizza_ingredients)
                recipeSteps!!.setText(R.string.pizza_steps)
                fries!!.visibility = View.GONE
                pizza!!.visibility = View.VISIBLE
            } else {
                ingredients!!.setText(R.string.french_fries_ingredients)
                recipeSteps!!.setText(R.string.french_fries_steps)
                fries!!.visibility = View.VISIBLE
                pizza!!.visibility = View.GONE
            }
        }

        override fun onTabUnselected(tab: TabLayout.Tab) {}
        override fun onTabReselected(tab: TabLayout.Tab) {}
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}