package com.example.pamo

class Question {
    var id = 0
    var question: String? = null
    var answers: List<String?>? = null
    var correct_answer: String? = null
    override fun toString(): String {
        return "Question{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", answers=" + answers +
                ", correct_answer='" + correct_answer + '\'' +
                '}'
    }
}