## PAMO

Programowanie Aplikacji Mobilnych (PJATK)

## Lab1: BMI calculator

<img src="lab1_bmi/lab1_screenshots/lab1_bmi_calc.PNG" alt="lab1_bmi_calc.PNG" width="220"/>
<img src="lab1_bmi/lab1_screenshots/lab1_bmi_calc_result.PNG" alt="lab1_bmi_calc_result.PNG" width="220"/>

## Lab2: Home / Menu / BMR calculator / Recipes

<img src="lab2_newScreens/lab2_screenshots/lab2_home.PNG" alt="lab2_home.PNG" width="220"/>
<img src="lab2_newScreens/lab2_screenshots/lab2_bmi.PNG" alt="lab2_bmi.PNG" width="220"/>
<img src="lab2_newScreens/lab2_screenshots/lab2_bmi2.PNG" alt="lab2_bmi2.PNG" width="220"/>
<img src="lab2_newScreens/lab2_screenshots/lab2_bmr.PNG" alt="lab2_bmr.PNG" width="220"/>
<img src="lab2_newScreens/lab2_screenshots/lab2_bmr2.PNG" alt="lab2_bmr2.PNG" width="220"/>
<img src="lab2_newScreens/lab2_screenshots/lab2_recipes.PNG" alt="lab2_recipes.PNG" width="220"/>
<img src="lab2_newScreens/lab2_screenshots/lab2_recipes2.PNG" alt="lab2_recipes2.PNG" width="220"/>

## Lab3: Quiz / BMI diagram

<img src="lab3_quiz/lab3_screenshots/lab3_quiz.PNG" alt="lab3_quiz.PNG" width="220"/>
<img src="lab3_quiz/lab3_screenshots/lab3_quiz_incorrect.PNG" alt="lab3_quiz_incorrect.PNG" width="220"/>
<img src="lab3_quiz/lab3_screenshots/lab3_quiz_correct.PNG" alt="lab3_quiz_correct.PNG" width="220"/>
<img src="lab3_quiz/lab3_screenshots/lab3_bmi_diagram.PNG" alt="lab3_bmi_diagram.PNG" width="220"/>
<img src="lab3_quiz/lab3_screenshots/lab3_bmi_diagram2.PNG" alt="lab3_bmi_diagram2.PNG" width="220"/>

## Lab4: Kotlin

<img src="lab4_kotlin/lab4_screenshots/lab4_kotlin.PNG" alt="lab4_kotlin.PNG" width="440"/>
