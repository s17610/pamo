package com.example.pamo;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.pamo.databinding.FragmentBmiBinding;

import java.text.NumberFormat;

public class BmiFragment extends Fragment {

    private FragmentBmiBinding binding;

    // number formatter object
    private static final NumberFormat numberFormat =
            NumberFormat.getNumberInstance();

    private double weight = 0.0; // weight (kilograms) entered by the user
    private double height = 0.0; // height (meters) entered by the user
    private TextView weightTextView; // shows formatted weight value
    private TextView heightTextView; // shows formatted height value
    private TextView bmiTextView; // shows calculated BMI
    private FrameLayout bmiFrameLayout;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentBmiBinding.inflate(inflater, container, false);

        // get references to programmatically manipulated TextViews
        weightTextView = binding.weightTextView;
        heightTextView = binding.heightTextView;
        bmiTextView = binding.bmiTextView;
        bmiTextView.setText(numberFormat.format(0.00));
        WebView bmiWebView = binding.bmiChartWebView;
        Button btn = binding.bmiChartButton;
        bmiFrameLayout = binding.bmiFrameLayout;

        //set listeners
        btn.setOnClickListener(btnClick);

        //enabling JavaScript for WebView
        WebSettings webSettings = bmiWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Create an unencoded HTML string
        // then convert the unencoded HTML string into bytes, encode
        // it with Base64, and load the data.
        String unencodedHtml =
                "<html>\n" +
                        "  <head>\n" +
                        "    <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                        "    <script type=\"text/javascript\">\n" +
                        "        google.charts.load('current', {'packages': ['corechart']});\n" +
                        "        google.charts.setOnLoadCallback(drawChart);\n" +
                        "\n" +
                        "        function drawChart() {\n" +
                        "\n" +
                        "          var data = new google.visualization.DataTable();\n" +
                        "          data.addColumn('date', 'Time of Day');\n" +
                        "          data.addColumn('number', 'BMI');\n" +
                        "\n" +
                        "          data.addRows([\n" +
                        "            [new Date(2022, 0, 1), 24.9],\n" +
                        "            [new Date(2022, 0, 2), 24.9],\n" +
                        "            [new Date(2022, 0, 3), 25],\n" +
                        "            [new Date(2022, 0, 4), 25.1],\n" +
                        "            [new Date(2022, 0, 5), 25.2],\n" +
                        "            [new Date(2022, 0, 6), 25.2],\n" +
                        "            [new Date(2022, 0, 7), 25.3],\n" +
                        "            [new Date(2022, 0, 8), 25.4],\n" +
                        "            [new Date(2022, 0, 9), 25.5],\n" +
                        "            [new Date(2022, 0, 10), 25.6],\n" +
                        "            [new Date(2022, 0, 11), 25.6],\n" +
                        "            [new Date(2022, 0, 12), 25.6],\n" +
                        "            [new Date(2022, 0, 13), 25.7],\n" +
                        "            [new Date(2022, 0, 14), 25.9],\n" +
                        "            [new Date(2022, 0, 15), 26],\n" +
                        "            [new Date(2022, 0, 16), 26.1],\n" +
                        "            [new Date(2022, 0, 17), 26.1],\n" +
                        "            [new Date(2022, 0, 18), 26.1],\n" +
                        "            [new Date(2022, 0, 19), 26.1],\n" +
                        "            [new Date(2022, 0, 20), 26.3],\n" +
                        "            [new Date(2022, 0, 21), 26.4],\n" +
                        "            [new Date(2022, 0, 22), 26.5],\n" +
                        "            [new Date(2022, 0, 23), 26.5],\n" +
                        "            [new Date(2022, 0, 24), 26.4],\n" +
                        "            [new Date(2022, 0, 25), 26.3],\n" +
                        "            [new Date(2022, 0, 26), 26.3],\n" +
                        "            [new Date(2022, 0, 27), 26],\n" +
                        "            [new Date(2022, 0, 28), 25.9],\n" +
                        "            [new Date(2022, 0, 29), 25.6],\n" +
                        "            [new Date(2022, 0, 30), 25.6],\n" +
                        "            [new Date(2022, 0, 31), 25.3],\n" +
                        "            [new Date(2022, 1, 1), 25],\n" +
                        "            [new Date(2022, 1, 2), 25.2]\n" +
                        "          ]);\n" +
                        "\n" +
                        "          var options = {\n" +
                        "            title: 'Zmiana BMI w czasie',\n" +
                        "            width: 400,\n" +
                        "            height: 500,\n" +
                        "            hAxis: {\n" +
                        "              format: 'MMM dd, yyyy',\n" +
                        "              gridlines: {\n" +
                        "                count: 15\n" +
                        "              }\n" +
                        "            },\n" +
                        "            vAxis: {\n" +
                        "              gridlines: {\n" +
                        "                color: 'none'\n" +
                        "              },\n" +
                        "              minValue: 20\n" +
                        "            }\n" +
                        "          };\n" +
                        "\n" +
                        "          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));\n" +
                        "\n" +
                        "          chart.draw(data, options);\n" +
                        "\n" +
                        "        }\n" +
                        "    </script>\n" +
                        "  </head>\n" +
                        "  <body>\n" +
                        "    <div id=\"chart_div\"></div>\n" +
                        "  </body>\n" +
                        "</html>";
        String encodedHtml = Base64.encodeToString(unencodedHtml.getBytes(),
                Base64.NO_PADDING);
        bmiWebView.loadData(encodedHtml, "text/html", "base64");

        // set weightEditText's TextWatcher
        EditText weightEditText = binding.weightEditText;
        weightEditText.addTextChangedListener(weightEditTextWatcher);

        // set heightEditText's TextWatcher
        EditText heightEditText = binding.heightEditText;
        heightEditText.addTextChangedListener(heightEditTextWatcher);

        return binding.getRoot();
    }

    //Button listener
    private final View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick (View view) {
            bmiFrameLayout.setVisibility(View.VISIBLE);
        }
    };

    // calculate and display BMI value
    private void calculate() {

        // calculate BMI
        double bmi = weight / (height * height);

        // display BMI
        bmiTextView.setText(numberFormat.format(bmi));
    }

    // listener object for the EditText's text-changed events
    private final TextWatcher weightEditTextWatcher = new TextWatcher() {
        // called when the user modifies the weight value
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try { // get weight value and display number formatted value
                weight = Double.parseDouble(s.toString()) / 100.0;
                weightTextView.setText(numberFormat.format(weight));
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                weightTextView.setText("");
                weight = 0.0;
            }

            calculate(); // update the bmiTextView
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    private final TextWatcher heightEditTextWatcher = new TextWatcher() {
        // called when the user modifies the height value
        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            try { // get height value and display number formatted value
                height = Double.parseDouble(s.toString()) / 100.0;
                heightTextView.setText(numberFormat.format(height));
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                heightTextView.setText("");
                height = 0.0;
            }

            calculate(); // update the bmiTextView
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(
                CharSequence s, int start, int count, int after) { }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}