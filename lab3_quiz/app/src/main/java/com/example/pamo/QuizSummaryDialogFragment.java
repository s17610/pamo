package com.example.pamo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class QuizSummaryDialogFragment extends DialogFragment {

    private Button resetQuizBtn;
    private TextView quizSummary, quizSummaryContent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_quiz_summary, container, false);
        resetQuizBtn = view.findViewById(R.id.resetQuizBtn);
        quizSummary = view.findViewById(R.id.quizSummaryTextView);
        quizSummaryContent = view.findViewById(R.id.quizSummaryContentTextView);

        resetQuizBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetQuizBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("resetQuizBtn", "Clicked resetQuizBtn");
                    }
                });

            }
        });

        return view;
    }
}
