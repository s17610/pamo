package com.example.pamo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.example.pamo.databinding.FragmentQuizBinding;

import java.lang.reflect.Type;
import java.util.List;

public class QuizFragment extends Fragment {

    private static final int GUESS_ROWS = 2;

    private Animation shakeAnimation; // animation for incorrect guess
    private Handler handler; // used to delay loading next flag
    private LinearLayout quizLinearLayout; // layout that contains the quiz
    private LinearLayout[] guessLinearLayouts; // rows of answer Buttons
    private TextView questionNumberTextView; // shows current question #
    private TextView questionContentTextView; // shows current question content
    private TextView answerTextView; // displays correct answer
    private int totalGuesses; // number of guesses made
    private int correctAnswers; // number of correct guesses
    private List<Question> questions; // list of quiz questions

    private FragmentQuizBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentQuizBinding.inflate(inflater, container, false);

        // load the shake animation that's used for incorrect answers
        shakeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.incorrect_shake);
        shakeAnimation.setRepeatCount(3); // animation repeats 3 times

        handler = new Handler();

        // get references to programmatically manipulated TextViews
        quizLinearLayout = binding.quizLinearLayout;
        guessLinearLayouts = new LinearLayout[2];
        guessLinearLayouts[0] = binding.row1LinearLayout;
        guessLinearLayouts[1] = binding.row2LinearLayout;
        questionNumberTextView = binding.questionNumberTextView;
        questionContentTextView = binding.questionContentTextView;
        answerTextView = binding.answerTextView;

        // configure listeners for the guess Buttons
        for (LinearLayout row : guessLinearLayouts) {
            for (int column = 0; column < row.getChildCount(); column++) {
                Button button = (Button) row.getChildAt(column);
                button.setOnClickListener(guessButtonListener);
            }
        }

        // getting questions data from JSON file
        String jsonFileString = Utils.getJsonFromAssets(getContext(), "quiz_questions.json");
        Log.i("quiz_questions_data", jsonFileString);
        Gson gson = new Gson();
        Type listQuestionType = new TypeToken<List<Question>>() { }.getType();
        questions = gson.fromJson(jsonFileString, listQuestionType);

        // set questionNumberTextView's text
        questionNumberTextView.setText(getString(R.string.question, 1, questions.size()));

        startQuiz();

        return binding.getRoot();
    }

    // set up and start the quiz
    public void startQuiz() {
        correctAnswers = 0; // reset the number of correct answers made
        totalGuesses = 0; // reset the total number of guesses the user made
        loadNextQuestion(); // start the quiz by loading the first flag
    }

    // after the user guesses a correct flag, load the next flag
    private void loadNextQuestion() {
        questionContentTextView.setText(""); // clear questionContentTextView
        answerTextView.setText(""); // clear answerTextView

        // display current question number
        questionNumberTextView.setText(getString(
                R.string.question, (correctAnswers + 1), questions.size()));

        // display current question content
        questionContentTextView.setText(questions.get(correctAnswers).question);

        // add 2, 4, 6 or 8 guess Buttons based on the value of GUESS_ROWS
        for (int row = 0; row < GUESS_ROWS; row++) {
            // place Buttons in currentTableRow
            for (int column = 0;
                 column < guessLinearLayouts[row].getChildCount();
                 column++) {
                // get reference to Button to configure
                Button newGuessButton =
                        (Button) guessLinearLayouts[row].getChildAt(column);
                newGuessButton.setEnabled(true);
                // set answers as newGuessButton's text
                newGuessButton.setText(
                        questions.get(correctAnswers).answers.get((row * 2) + column));
            }
        }
    }

    // utility method that disables all answer Buttons
    private void disableButtons() {
        for (int row = 0; row < GUESS_ROWS; row++) {
            LinearLayout guessRow = guessLinearLayouts[row];
            for (int i = 0; i < guessRow.getChildCount(); i++)
                guessRow.getChildAt(i).setEnabled(false);
        }
    }

    // animates the entire quizLinearLayout on or off screen
    private void animate(boolean animateOut) {
        // prevent animation into the the UI for the first flag
        if (correctAnswers == 0)
            return;

        // calculate center x and center y
        int centerX = (quizLinearLayout.getLeft() +
                quizLinearLayout.getRight()) / 2; // calculate center x
        int centerY = (quizLinearLayout.getTop() +
                quizLinearLayout.getBottom()) / 2; // calculate center y

        // calculate animation radius
        int radius = Math.max(quizLinearLayout.getWidth(),
                quizLinearLayout.getHeight());

        Animator animator;

        // if the quizLinearLayout should animate out rather than in
        if (animateOut) {
            // create circular reveal animation
            animator = ViewAnimationUtils.createCircularReveal(
                    quizLinearLayout, centerX, centerY, radius, 0);
            animator.addListener(
                    new AnimatorListenerAdapter() {
                        // called when the animation finishes
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            loadNextQuestion();
                        }
                    }
            );
        }
        else { // if the quizLinearLayout should animate in
            animator = ViewAnimationUtils.createCircularReveal(
                    quizLinearLayout, centerX, centerY, 0, radius);
        }

        animator.setDuration(500); // set animation duration to 500 ms
        animator.start(); // start the animation
    }

    // called when a guess Button is touched
    private View.OnClickListener guessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button guessButton = ((Button) v);
            String guess = guessButton.getText().toString();
            String answer = questions.get(correctAnswers).correct_answer;
            ++totalGuesses; // increment number of guesses the user has made

            if (guess.equals(answer)) { // if the guess is correct
                ++correctAnswers; // increment the number of correct answers

                // display correct answer in green text
                answerTextView.setText(answer + "!");
                answerTextView.setTextColor(
                        getResources().getColor(R.color.correct_answer,
                                getContext().getTheme()));

                disableButtons(); // disable all guess Buttons

                // if the user has correctly answered all questions
                if (correctAnswers == questions.size()) {
                    // DialogFragment to display quiz stats and start quiz
                    DialogFragment quizResults = new QuizSummaryDialogFragment();
                    quizResults.setCancelable(false);
                    quizResults.show(getChildFragmentManager(), "quiz results");

/*                    DialogFragment quizResults =
                            new QuizSummaryDialogFragment() {
                                // create an AlertDialog and return it
                                @NonNull
                                @Override
                                public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
                                    return new AlertDialog.Builder(requireContext())
                                            .setMessage(getString(R.string.results,
                                                    totalGuesses, (1000 / (double) totalGuesses)))
                                            .setPositiveButton(R.string.reset_quiz,
                                                    (dialog, id) -> startQuiz())
                                            .create();
                                }
                            };*/

                    // use FragmentManager to display the DialogFragment
                    //quizResults.setCancelable(false);
                    //quizResults.show(getChildFragmentManager(), "quiz results");
                }
                else { // answer is correct but quiz is not over
                    // load the next flag after a 2-second delay
                    handler.postDelayed(
                            () -> {
                                animate(true); // animate the question off the screen
                            }, 2000); // 2000 milliseconds for 2-second delay
                }
            }
            else { // answer was incorrect
                questionContentTextView.startAnimation(shakeAnimation); // play shake

                // display "Incorrect!" in red
                answerTextView.setText(R.string.incorrect_answer);
                answerTextView.setTextColor(getResources().getColor(
                        R.color.incorrect_answer, getContext().getTheme()));
                guessButton.setEnabled(false); // disable incorrect answer
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
